package com.fsl.ocr

import com.amazonaws.client.builder.AwsClientBuilder
import com.amazonaws.services.s3.AmazonS3ClientBuilder
import org.apache.pdfbox.pdmodel.PDDocument
import org.apache.pdfbox.rendering.ImageType
import org.apache.pdfbox.rendering.PDFRenderer
import org.apache.pdfbox.tools.imageio.ImageIOUtil
import java.io.File
import kotlin.system.measureTimeMillis


fun File.uploadToS3(bucket: String, forceUpload: Boolean = false): String{
    val document = this.name
    val s3client = AmazonS3ClientBuilder.standard()
        .withEndpointConfiguration(
            AwsClientBuilder.EndpointConfiguration("https://s3.amazonaws.com", "eu-west-1")
        )
        .build()
    val keyS3File = "sample/$document"

    if (!s3client.doesObjectExist(bucket, keyS3File) || forceUpload) {

        println("Uploading $document to S3 Bucket $bucket")
        val uploadtime = measureTimeMillis {
            s3client.putObject(bucket, keyS3File, this)
        }
        println("Uploaded $document to S3 Bucket $bucket in $uploadtime")
    }else{
        println("Document $document already in S3 Bucket $bucket")
    }
    return keyS3File
}

/**
 * Files extension function
 * Transform a pdf file in to a list of Tiff images and return the list of Files generated
 */
fun File.pdf2tiff(outputDir: String = "./tmp"): List<File> {
    val extension: String = "tiff"
    var listFiles = mutableListOf<File>()

    //create outputDirectory
    File(outputDir).mkdirs()

    val document: PDDocument = PDDocument.load(this)
    val pdfRenderer = PDFRenderer(document)
    for (page in 0 until document.numberOfPages) {
        val bim = pdfRenderer.renderImageWithDPI(page, 300f, ImageType.BINARY)

        val filePath = "$outputDir/${this.name}-$page"
        val fileName = "$outputDir/${this.name}-$page.$extension"
        ImageIOUtil.writeImage(
            bim, extension,java.lang.String.format(filePath), 300
        )
        listFiles.add(File(fileName))
    }

    document.close()

    return listFiles
}

fun File.pdf2png(outputDir: String = "./tmp"): List<File> {
    val extension: String = "png"
    var listFiles = mutableListOf<File>()

    //create outputDirectory
    File(outputDir).mkdirs()

    val document: PDDocument = PDDocument.load(this)
    val pdfRenderer = PDFRenderer(document)
    for (page in 0 until document.numberOfPages) {
        val bim = pdfRenderer.renderImageWithDPI(page, 300f, ImageType.BINARY)

        val filePath = "$outputDir/${this.name}-$page"
        val fileName = "$outputDir/${this.name}-$page.$extension"
        ImageIOUtil.writeImage(
            bim, "png",java.lang.String.format(filePath), 300
        )
        listFiles.add(File(fileName))
    }

    document.close()

    return listFiles
}

fun File.isPdf() = this.extension.endsWith("pdf")


fun File.validFileFormat (): Boolean{
    val validFileFormat = setOf("png", "jpg", "jpeg", "tiff", "pdf")
    return this.isFile && this.extension in validFileFormat
}