package com.fsl.ocr

import com.amazonaws.services.textract.model.DetectDocumentTextResult
import java.io.File


fun DetectDocumentTextResult.toTextFile(fileName: String) {

    val blocks = this.blocks
    File(fileName).bufferedWriter().use { out ->
        blocks.forEach {
            if (it.getBlockType() == "LINE") {
                out.write(it.text)
                out.newLine()
            }
        }
    }
}
