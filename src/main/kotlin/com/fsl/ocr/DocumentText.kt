package com.fsl.ocr

import com.amazonaws.auth.policy.*
import com.amazonaws.auth.policy.actions.SQSActions
import com.amazonaws.client.builder.AwsClientBuilder.EndpointConfiguration
import com.amazonaws.services.sns.AmazonSNS
import com.amazonaws.services.sns.AmazonSNSClientBuilder
import com.amazonaws.services.sns.model.CreateTopicRequest
import com.amazonaws.services.sns.model.CreateTopicResult
import com.amazonaws.services.sqs.AmazonSQS
import com.amazonaws.services.sqs.AmazonSQSClientBuilder
import com.amazonaws.services.sqs.model.CreateQueueRequest
import com.amazonaws.services.sqs.model.Message
import com.amazonaws.services.sqs.model.QueueAttributeName
import com.amazonaws.services.sqs.model.SetQueueAttributesRequest
import com.amazonaws.services.textract.AmazonTextract
import com.amazonaws.services.textract.AmazonTextractClientBuilder
import com.amazonaws.services.textract.model.*
import com.fasterxml.jackson.databind.ObjectMapper
import kotlinx.coroutines.asCoroutineDispatcher
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import picocli.CommandLine
import java.io.File
import java.util.*
import java.util.concurrent.Callable
import java.util.concurrent.Executors
import kotlin.system.exitProcess


enum class ProcessType {
    DETECTION, ANALYSIS
}

@CommandLine.Command(
    name = "textExtract", mixinStandardHelpOptions = true, version = ["textExtract 1.0"],
    description = ["Text Extract of image files"]
)
class TextExtract : Callable<Int> {

    @CommandLine.Parameters(index = "0", description = ["The file whose text we want to extract"])
    lateinit var file: File

    @CommandLine.Option(names = ["-b", "--bucket"], description = ["s3 bucket to store files"])
    lateinit var inBucket: String

    @CommandLine.Option(names = ["-d", "--dir"], description = ["directory to out files"])
    var outputDir = "./out"

    @CommandLine.Option(names = ["-f", "--format"], description = ["image out format allowed png|tiff from pdf files"])
    var imageFormat = "png"

    @CommandLine.Option(names = ["-a", "--async"], description = ["Use Async AWS integration"], negatable = false)
    var async :Boolean = false

    var sqs: AmazonSQS
    var sns: AmazonSNS
    var textract: AmazonTextract

    private var sqsQueueName: String? = null
    private var snsTopicName: String? = null
    private var snsTopicArn: String? = null
    private var roleArn: String? = null
    private var sqsQueueUrl: String? = null
    private var sqsQueueArn: String? = null

    init {
        // Call DetectDocumentText
        val endpoint = EndpointConfiguration(
            "https://textract.eu-west-1.amazonaws.com", "eu-west-1"
        )
        textract = AmazonTextractClientBuilder.standard().withEndpointConfiguration(endpoint).build()
        sns = AmazonSNSClientBuilder.defaultClient()
        sqs = AmazonSQSClientBuilder.defaultClient()

        roleArn = "arn:aws:iam::822978072221:group/TextExtractRoleGroup"
    }

    fun createTopicAndQueue() {

        //create a new SNS topic
        snsTopicName = "AmazonTextractTopic" + java.lang.Long.toString(System.currentTimeMillis())
        val createTopicRequest = CreateTopicRequest(snsTopicName)
        val createTopicResult: CreateTopicResult = sns.createTopic(createTopicRequest)
        snsTopicArn = createTopicResult.getTopicArn()

        //Create a new SQS Queue
        sqsQueueName = "AmazonTextractQueue" + java.lang.Long.toString(System.currentTimeMillis())
        val createQueueRequest = CreateQueueRequest(sqsQueueName)
        sqsQueueUrl = sqs.createQueue(createQueueRequest).getQueueUrl()
        sqsQueueArn = sqs.getQueueAttributes(sqsQueueUrl, Arrays.asList("QueueArn")).getAttributes().get("QueueArn")

        //Subscribe SQS queue to SNS topic
        val sqsSubscriptionArn: String = sns.subscribe(snsTopicArn, "sqs", sqsQueueArn).getSubscriptionArn()

        // Authorize queue
        val policy: Policy = Policy().withStatements(
            Statement(Statement.Effect.Allow)
                .withPrincipals(Principal.AllUsers)
                .withActions(SQSActions.SendMessage)
                .withResources(Resource(sqsQueueArn))
                .withConditions(
                    Condition().withType("ArnEquals").withConditionKey("aws:SourceArn").withValues(snsTopicArn)
                )
        )


        val queueAttributes: MutableMap<String, String> = HashMap<String, String>()
        queueAttributes[QueueAttributeName.Policy.toString()] = policy.toJson()
        sqs.setQueueAttributes(SetQueueAttributesRequest(sqsQueueUrl, queueAttributes))


        println("Topic arn: $snsTopicArn")
        println("Queue arn: $sqsQueueArn")
        println("Queue url: $sqsQueueUrl")
        println("Queue sub arn: $sqsSubscriptionArn")
    }

    fun startDocumentTextractAsync(bucket: String, keyS3File: String): String {
        //Create notification channel
        val channel = NotificationChannel()
            .withSNSTopicArn(snsTopicArn)
            .withRoleArn(roleArn)

        val requestAsync = StartDocumentTextDetectionRequest()
            .withDocumentLocation(DocumentLocation().withS3Object(S3Object().withName(keyS3File).withBucket(bucket)))
            //.withOutputConfig()
            .withJobTag("DetectingText")
            .withNotificationChannel(channel)

        val startDocumentTextDetectionResult = textract.startDocumentTextDetection(requestAsync)

        return startDocumentTextDetectionResult.getJobId()
    }

    fun processDocument(inBucket: String, inDocument: String, inRole: String, type: ProcessType) {

        val awsTextract = AwsTextract()

        var startJobId = ""
        when (type) {
            ProcessType.DETECTION -> {
                startJobId = startDocumentTextractAsync(inBucket, inDocument)
                println("Processing type: Detection")
            }
            ProcessType.ANALYSIS -> {
                //  StartDocumentAnalysis(bucket, document)
                println("Processing type: Analysis")
            }
        }
        println("Waiting for job: $startJobId")
        //Poll queue for messages
        var messages: List<Message>? = null
        var dotLine = 0
        var jobFound = false

        //loop until the job status is published. Ignore other messages in queue.
        do {
            messages = sqs.receiveMessage(sqsQueueUrl).messages
            if (dotLine++ < 40) {
                print(".")
            } else {
                println()
                dotLine = 0
            }
            if (!messages.isEmpty()) {
                //Loop through messages received.
                for (message in messages) {
                    val notification = message.body

                    // Get status and job id from notification.
                    val mapper = ObjectMapper()
                    val jsonMessageTree = mapper.readTree(notification)
                    val messageBodyText = jsonMessageTree["Message"]
                    val operationResultMapper = ObjectMapper()
                    val jsonResultTree = operationResultMapper.readTree(messageBodyText.textValue())
                    val operationJobId = jsonResultTree["JobId"]
                    val operationStatus = jsonResultTree["Status"]
                    println("Job found was $operationJobId")
                    // Found job. Get the results and display.
                    if (operationJobId.asText() == startJobId) {
                        jobFound = true
                        println("Job id: $operationJobId")
                        println("Status : $operationStatus")
                        if (operationStatus.asText() == "SUCCEEDED") {
                            when (type) {
                                ProcessType.DETECTION -> awsTextract.getDocumentTextDetectionResults(startJobId)
                                // ProcessType.ANALYSIS -> GetDocumentAnalysisResults()
                                else -> {
                                    println("Invalid processing type. Choose Detection or Analysis")
                                    throw java.lang.Exception("Invalid processing type")
                                }
                            }
                        } else {
                            println("Document analysis failed")
                        }
                        sqs.deleteMessage(sqsQueueUrl, message.receiptHandle)
                    } else {
                        System.out.println("Job received was not job $startJobId")
                        //Delete unknown message. Consider moving message to dead letter queue
                        sqs.deleteMessage(sqsQueueUrl, message.receiptHandle)
                    }
                }
            } else {
                Thread.sleep(5000)
            }
        } while (!jobFound)

        println("Finished processing document")


    }

    fun processDocumentAsync(bucket: String, document: String) {
        createTopicAndQueue()
        processDocument(bucket, document, roleArn.orEmpty(), ProcessType.DETECTION)
        deleteTopicandQueue()
        println("Done!")
    }


    fun deleteTopicandQueue() {
        if (sqs != null) {
            sqs.deleteQueue(sqsQueueUrl)
            println("SQS queue deleted")
        }
        if (sns != null) {
            sns.deleteTopic(snsTopicArn)
            println("SNS topic deleted")
        }
    }

    override fun call(): Int {

        // The S3 bucket and document
        if (this.async){
            println("Using AWS Async Mode")
            val keyS3File = file.uploadToS3(inBucket)
            processDocumentAsync(inBucket, keyS3File)
        }else {
            val awsTextExtract = AwsSyncTextExtract(inBucket, imageFormat )
            if (file.isFile) {
                //hacemos el proceso para 1 fichero
                awsTextExtract.processDocument(file, outputDir)
            } else {//is directory
                runBlocking(Executors.newFixedThreadPool(1).asCoroutineDispatcher()) {
                    file.walk().forEach {
                        launch {
                            awsTextExtract.processDocument(it, outputDir)
                        }
                    }
                }
            }
        }

        return 0
    }
}

fun main(args: Array<String>): Unit = exitProcess(CommandLine(TextExtract()).execute(*args))