package com.fsl.ocr

import com.amazonaws.client.builder.AwsClientBuilder
import com.amazonaws.services.textract.AmazonTextract
import com.amazonaws.services.textract.AmazonTextractClientBuilder
import com.amazonaws.services.textract.model.Block
import com.amazonaws.services.textract.model.GetDocumentTextDetectionRequest
import com.amazonaws.services.textract.model.GetDocumentTextDetectionResult


class AwsTextract {

    var textract : AmazonTextract

    init{
        // Call DetectDocumentText
        val endpoint = AwsClientBuilder.EndpointConfiguration(
            "https://textract.eu-west-1.amazonaws.com", "eu-west-1"
        )
        textract = AmazonTextractClientBuilder.standard().withEndpointConfiguration(endpoint).build()
    }

    //Gets the results of processing started by StartDocumentTextDetection
    @Throws(Exception::class)
    fun getDocumentTextDetectionResults(startJobId: String) {
        val maxResults = 1000
        var paginationToken: String? = null
        var response: GetDocumentTextDetectionResult?
        var finished = false
        while (finished == false) {
            val documentTextDetectionRequest = GetDocumentTextDetectionRequest()
                .withJobId(startJobId)
                .withMaxResults(maxResults)
                .withNextToken(paginationToken)
            response = textract.getDocumentTextDetection(documentTextDetectionRequest)
            val documentMetaData = response.documentMetadata
            println("Pages: " + documentMetaData.pages.toString())

            //Show blocks information
            val blocks: List<Block> = response.blocks
            for (block in blocks) {
                displayBlockInfo(block)
            }
            paginationToken = response.nextToken
            if (paginationToken == null) finished = true
        }
    }


    //Displays Block information for text detection and text analysis
    private fun displayBlockInfo(block: Block) {
        println("Block Id : " + block.id)
        if (block.text != null) println("\tDetected text: " + block.text)
        println("\tType: " + block.blockType)
        if (block.blockType == "PAGE" != true) {
            println("\tConfidence: " + block.confidence.toString())
        }
        if (block.blockType == "CELL") {
            println("\tCell information:")
            println("\t\tColumn: " + block.columnIndex)
            println("\t\tRow: " + block.rowIndex)
            println("\t\tColumn span: " + block.columnSpan)
            println("\t\tRow span: " + block.rowSpan)
        }
        println("\tRelationships")
        val relationships = block.relationships
        if (relationships != null) {
            for (relationship in relationships) {
                println("\t\tType: " + relationship.type)
                println("\t\tIDs: " + relationship.ids.toString())
            }
        } else {
            println("\t\tNo related Blocks")
        }
        println("\tGeometry")
        println("\t\tBounding Box: " + block.geometry.boundingBox.toString())
        println("\t\tPolygon: " + block.geometry.polygon.toString())
        val entityTypes = block.entityTypes
        println("\tEntity Types")
        if (entityTypes != null) {
            for (entityType in entityTypes) {
                println("\t\tEntity Type: $entityType")
            }
        } else {
            println("\t\tNo entity type")
        }
        if (block.blockType == "SELECTION_ELEMENT") {
            print("    Selection element detected: ")
            if (block.selectionStatus == "SELECTED") {
                println("Selected")
            } else {
                println(" Not selected")
            }
        }
        if (block.page != null) println("\tPage: " + block.page)
        println()
    }
}