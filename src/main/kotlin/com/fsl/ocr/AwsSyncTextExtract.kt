package com.fsl.ocr

import com.amazonaws.client.builder.AwsClientBuilder
import com.amazonaws.services.textract.AmazonTextract
import com.amazonaws.services.textract.AmazonTextractClientBuilder
import com.amazonaws.services.textract.model.DetectDocumentTextRequest
import com.amazonaws.services.textract.model.Document
import com.amazonaws.services.textract.model.S3Object
import java.io.File
import kotlin.system.measureTimeMillis

class AwsSyncTextExtract(val s3Bucket: String, val imageFormat: String = "png") {

    var textract : AmazonTextract

    init{
        // Call DetectDocumentText
        val endpoint = AwsClientBuilder.EndpointConfiguration(
            "https://textract.eu-west-1.amazonaws.com", "eu-west-1"
        )
        textract = AmazonTextractClientBuilder.standard().withEndpointConfiguration(endpoint).build()
    }

    fun extractText(file: File, outputDir: String){
        val keyS3File = file.uploadToS3(s3Bucket)
        val request = DetectDocumentTextRequest()
            .withDocument(Document().withS3Object(S3Object().withName(keyS3File).withBucket(s3Bucket)))

        val time_to_process_file = measureTimeMillis {
            val result = textract.detectDocumentText(request)
            result.toTextFile("$outputDir/${file.name}.txt")
        }
        println("File $keyS3File processed in $time_to_process_file milliseconds")
    }

    /**
     * For image documents and 1 page documents, Amazon allows to use Sync Methods
     *
     */
    fun processDocument(inputFile: File, outputDir: String){

        var outputDirectory = File(outputDir)
        if (!outputDirectory.exists()){
            println("Creating outputDirectory $outputDir")
            outputDirectory.mkdirs()
        }
        if (inputFile.validFileFormat()) {
            val filesToProcess = mutableListOf<File>()
            if (inputFile.isPdf()){
                println("Transforming pdf to $imageFormat ${inputFile.name}")

                val listFile = when (imageFormat) {
                    "png" -> inputFile.pdf2png("./tmp")
                    "tiff" -> inputFile.pdf2tiff("./tmp")
                    else -> inputFile.pdf2png("./tmp")
                }
                filesToProcess.addAll(listFile)
            }else{
                filesToProcess.add(inputFile)
            }

            filesToProcess.forEach {
                extractText(it,outputDir)
            }
        }
    }
}