Demo-Kotlin-aws-textextract
----

Kotlin Implementation of Integration Client to Amazon AWS TextExtract

### Precondition

- to have Java 8 at least instaled
- Have the Aws Credentials correctly configured at ~/.aws directory
- Create a S3 Bucket to store files processed by AWS TextExtract


### Compiling

    ./gradlew clean build


## Testing CLI

Go to distributions dir and extract the package

    cd ./build/distributions
    tar xzvf aws-textextract-1.0-SNAPSHOT.tar
    cd aws-textextract-1.0-SNAPSHOT/bin

Then you can Run 

    ./aws-textextract

You may see something similar to this

```
./aws-textextract
Missing required parameter: '<file>'
Usage: textExtract [-ahV] [-b=<inBucket>] [-d=<outputDir>] [-f=<imageFormat>]
                   <file>
Text Extract of image files
      <file>                The file whose text we want to extract
  -a, --async               Use Async AWS integration
  -b, --bucket=<inBucket>   s3 bucket to store files
  -d, --dir=<outputDir>     directory to out files
  -f, --format=<imageFormat>
                            image out format allowed png|tiff from pdf files
  -h, --help                Show this help message and exit.
  -V, --version             Print version information and exit.

```

## Process 1 page documents Using Syncronous mode

    ./aws-textextract {{one_page_document_to_process}}  -b {{name_of_s3_bucket}} -d ./out


## Process N  page documents Using Asyncronous mode

Note: this is not working yet

    ./aws-textextract {{any_page_document_to_process}} --async -b {{name_of_s3_bucket}} -d ./out

